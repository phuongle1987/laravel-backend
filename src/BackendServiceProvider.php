<?php

namespace Phuongle\Backend;

use Illuminate\Support\ServiceProvider;

class BackendServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
      if (! $this->app->routesAreCached()) {
        require __DIR__.'/../../routes.php';
      }
      $this->loadViewsFrom(__DIR__.'/path/to/views', 'courier');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
